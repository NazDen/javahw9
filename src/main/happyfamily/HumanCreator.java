package happyfamily;

public interface HumanCreator {

     Human bornChild(int year);

}
