package happyfamily;

public class Fish extends Pet {

    @Override
    void respond() {
        System.out.println("Буль-буль.");
    }

    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(Species.FISH);
    }
}
