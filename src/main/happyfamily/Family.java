package happyfamily;

import java.util.Arrays;
import java.util.Objects;

public class Family {

    static{
        System.out.println("Class "+ Family.class.getSimpleName()+" is loading.");
    }
    {
        System.out.println("New " + this.getClass().getSimpleName() +" class creating.");
    }

    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;


    Family(Human mother, Human father){
        this.mother= mother;
        this.father= father;
        mother.setFamily(this);
        father.setFamily(this);
        this.children= new Human[0];
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }


    @Override
    protected void finalize() throws Throwable {
        System.out.println(this);
        super.finalize();
    }

    @Override
    public String toString(){

        return this.getClass().getSimpleName()+"{mother= "+mother+", father= "+father+", children= "+Arrays.toString(children)+" pet= "+pet+"}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }

    public void addChild(Human child){
        Human[] temp= new Human[children.length+1];
        temp[temp.length-1]=child;
        for (int i = 0; i <children.length ; i++) {
            temp[i]=children[i];
        }
        children=temp;
        child.setFamily(this);
     }

     boolean deleteChild(Human smbdy){
        boolean delete=false;
        int indexChildren=0;
        int indexTemp=0;
        Human[] temp=new Human[children.length];
        for (Human child:children) {
            if (child.equals(smbdy)) {
                indexChildren++;
                child.setFamily(null);
                delete = true;
            } else {
                temp[indexTemp] = children[indexChildren];
                indexTemp++;
                indexChildren++;
            }
        }
         if (indexChildren!=indexTemp) {
             children=Arrays.copyOfRange(temp,0,indexTemp);
         }else {
       children=temp;}
        return delete;
     }

     int countFamily(){

         return 2 + children.length;
     }
}
